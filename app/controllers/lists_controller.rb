class ListsController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def create
    @task = Task.find(params[:task_id])
    @list = List.new(task: @task, user: current_user)
    
    if @list.save
      redirect_to root_path, notice: 'Tarea completada'
    else
      redirect_to root_path, alert: 'Acción no completada'
    end
  end

  def destroy
    # toma todos los valores con "take"
    @list = List.where(user_id: current_user.id, task_id: params[:task_id]).take

    if @list.destroy
      redirect_to root_path, notice: 'Tarea marcada como No Completada'
    else
      redirect_to root_path, alert: 'Acción no completada'
    end
  end
end
