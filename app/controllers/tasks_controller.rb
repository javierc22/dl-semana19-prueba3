class TasksController < ApplicationController
  before_action :authenticate_user!, only: [:index]

  def index
    @tasks = Task.all
  end

  def show
    @tasks = Task.all
    @task = Task.find(params[:id])
    @top5 = @task.lists.order(:created_at).limit(5)
  end
end
