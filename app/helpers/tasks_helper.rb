module TasksHelper
  # Método Helper para saber si la tarea fue completada o no
  def completed?(task, user)
    task.users.exists?(user.id) # Busca si en la tarea, existe un usuario con el id de usuario actual.
  end
end
