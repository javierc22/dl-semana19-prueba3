# Actividad 38 - Prueba Rails

## Historias de usuario:

1. Yo como usuario, desde el listado de tareas, debo poder ver todas las tareas para poder ir completando cada una.

2. Yo como usuario, desde el listado de tareas, debo poder marcar una tarea como completada para llevar un registro de mis tareas completadas, luego debo ser redirigido al listado de tareas.

3. Yo como usuario, desde el listado de tareas, debo poder ingresar al detalle de una tarea para ver los usuarios que la han completado.

4. Yo como usuario, desde el listado de tareas, puedo marcar como NO completada una tarea que se encontraba completada para poder corregir una que completépor error, luego debo ser redirigido al listado de tareas.

5. Yo como usuario, desde cualquier vista, debo poder ver la barra de navegación con el conteo de tareas completadas para saber cuantas tareas llevo completadas del total.

6. Yo como usuario, desde la vista de detalle de una tarea, puedo ver un ranking de los primeros cinco usuarios en completar la tarea junto con su nombre y foto de perfil, además de una lista simple de todos los usuarios que han completado la tarea.

## Requisitos No-funcionales:

Dentro de los datos de prueba (seeds.rb) se debe incluir, al menos, 9 tareas con
título e imagen (URL).