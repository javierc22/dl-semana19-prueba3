# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Task.destroy_all

tareas = [
  {
    title: 'Bailar Cueca',
    description: 'Bailar un pie de cueca con las chiquillas :v',
    image: 'cueca.jpg'
  },
  {
    title: 'Jugar al trompo',
    description: 'Jugar al trompo y destrozar los trompos :v',
    image: 'trompo.jpg'
  },
  {
    title: 'Comer Empanadas',
    description: 'Comer empanadas y más empanadas!!',
    image: 'empanada.jpg'
  },
  {
    title: 'Ir a una Fonda',
    description: 'Tiquitiquiti! Una fonda es el lugar ideal para pasar el 18!',
    image: 'fonda.jpg'
  },
  {
    title: 'Elevar Volantín',
    description: 'Hechar a volar muchos volantines',
    image: 'volantin.jpg'
  },
  {
    title: 'Tomar un Terremoto',
    description: 'Un terremotito :v',
    image: 'terremoto.jpg'
  },
  {
    title: 'Jugar una pichanga',
    description: 'Jugar una pichanga con los amigos!',
    image: 'pichanga.jpg'
  },
  {
    title: 'Descansar!',
    description: 'A dormir se ha dicho!',
    image: 'dormir.gif'
  }
]

tareas.each do |tarea|
  Task.create(
    title: tarea[:title],
    description: tarea[:description],
    image: tarea[:image]
  )
end